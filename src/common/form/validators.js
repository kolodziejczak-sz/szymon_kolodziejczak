export default {
  required: {
    msg: 'The field is required',
    isValid(value) {
      if (value === undefined || value === null) return false;
      if (typeof value === 'string') {
        return value.trim().length > 0;
      }
      return true;
    }
  }
};
