import React from 'react';
import PropTypes from 'prop-types';
import FormControl from './FormControl';

class Form extends React.Component {
  formControlRefs = [];

  onSubmit = e => {
    e.preventDefault();
    e.stopPropagation();

    const formControls = this.formControlRefs.map(ref => ref.current.validate());
    const isFormValid = formControls.every(fc => fc.isValid);
    const formValue = formControls.reduce((formValue, fc) => {
      formValue[fc.name] = fc.value;
      return formValue;
    }, {});

    if (this.props.onSubmit) {
      this.props.onSubmit({
        isValid: isFormValid,
        value: formValue
      });
    }
  };

  render = () => {
    this.formControlRefs = [];

    const children = [].concat(this.props.children).map((c, idx) => {
      const isFormControl = c && c.type.name === FormControl.name;

      if (isFormControl) {
        const ref = React.createRef();
        this.formControlRefs.push(ref);
        return React.cloneElement(c, { key: idx, ref });
      }
      return c;
    });

    return (
      <form onSubmit={this.onSubmit} noValidate>
        {children}
      </form>
    );
  };
}

Form.propTypes = {
  onSubmit: PropTypes.func,
  children: PropTypes.node
};

export default Form;
