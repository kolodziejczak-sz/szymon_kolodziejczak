import React from 'react';
import PropTypes from 'prop-types';
import { If } from '../components';
import validators from './validators';
import { getClassNameWithModifier } from '../utils';

class FormControl extends React.Component {
  state = {
    isPristine: true,
    isValid: true,
    name: this.props.name,
    value: this.props.defaultValue
  };

  handleChange = e => {
    this.validate(e.target.value);
  };

  validate = (value = this.state.value) => {
    const validator = this.props.validator;
    const isValid = validator ? validator.isValid(value) : true;
    const newState = {
      ...this.state,
      value,
      isValid,
      isPristine: false
    };
    this.setState(newState);
    return newState;
  };

  render = () => {
    const { id, name, label, defaultValue, validator = {}, children } = this.props;
    const { isValid, isPristine } = this.state;
    const isRequired = Boolean(validator === validators.required);
    const shouldDisplayInvalidState = !isValid && !isPristine;

    const Input = React.cloneElement(children, {
      id,
      name,
      defaultValue,
      onBlur: this.handleChange,
      onChange: this.handleChange,
      className: shouldDisplayInvalidState ? 'is-invalid' : ''
    });

    return (
      <div className="form-group">
        <If cond={!!label}>
          <label
            className={getClassNameWithModifier('form-group-label', 'required')(isRequired)}
            htmlFor={id}
          >
            {label}
          </label>
        </If>
        {Input}
        <If cond={!!validator}>
          <div className="invalid-feedback">{validator.msg}</div>
        </If>
      </div>
    );
  };
}

FormControl.propTypes = {
  name: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  children: PropTypes.element.isRequired,
  validator: PropTypes.shape({
    msg: PropTypes.string.isRequired,
    isValid: PropTypes.func.isRequired,
    defaultValue: PropTypes.any
  })
};

export default FormControl;
