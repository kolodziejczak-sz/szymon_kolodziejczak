import validators from '../validators';

describe('common/form: validators', () => {
  describe('required validator', () => {
    const validator = validators.required.isValid;

    test('given empty values returns false', () => {
      let values = [undefined, '', '   ', null];
      for (let value of values) {
        expect(validator(value)).toBe(false);
      }
    });

    test('given non-empty values returns true', () => {
      let values = [true, 'any string', false];
      for (let value of values) {
        expect(validator(value)).toBe(true);
      }
    });
  });
});
