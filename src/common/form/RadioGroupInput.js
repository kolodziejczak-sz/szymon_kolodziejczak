import React from 'react';
import PropTypes from 'prop-types';
import { If } from '../components';

const RadioGroupInput = ({ name, list, defaultValue, className = '', ...attrs }) => {
  return (
    <div className={'form-control ' + className}>
      <If cond={!!list}>
        {list.map(({ label, value }, idx) => {
          let { id, ...attrsWithoutId } = attrs;
          id = name + idx;
          return (
            <div className="form-check form-check-inline" key={idx}>
              <label htmlFor={id} className="form-check-input">
                {label}
              </label>
              <input
                id={id}
                className={`form-check-input mb-1 ${className}`}
                type="radio"
                value={value}
                defaultChecked={value === defaultValue}
                {...attrsWithoutId}
              />
            </div>
          );
        })}
      </If>
    </div>
  );
};

RadioGroupInput.propTypes = {
  name: PropTypes.string,
  defaultValue: PropTypes.string,
  list: PropTypes.arrayOf(
    PropTypes.shape({
      label: PropTypes.string.isRequired,
      value: PropTypes.string.isRequired
    })
  )
};

export default RadioGroupInput;
