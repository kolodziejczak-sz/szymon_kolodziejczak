import React from 'react';
import PropTypes from 'prop-types';
import If from '../components/If';

const SelectInput = ({ className = '', options, defaultValue, ...attrs }) => (
  <select className={'form-control ' + className} value={defaultValue} {...attrs}>
    <option value="">Select value</option>
    <If cond={!!options}>
      {options.map((o, idx) => (
        <option key={idx} value={o.value}>
          {o.label}
        </option>
      ))}
    </If>
  </select>
);

SelectInput.propTypes = {
  className: PropTypes.string,
  defaultValue: PropTypes.string,
  options: PropTypes.arrayOf(
    PropTypes.shape({
      label: PropTypes.string.isRequired,
      value: PropTypes.string.isRequired
    })
  )
};

export default SelectInput;
