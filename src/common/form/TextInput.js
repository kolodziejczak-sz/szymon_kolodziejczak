import React from 'react';
import PropTypes from 'prop-types';

const TextInput = ({ className = '', ...attrs }) => {
  return <input className={'form-control ' + className} type="text" {...attrs} />;
};

TextInput.propTypes = {
  className: PropTypes.string,
  attrs: PropTypes.any
};

export default TextInput;
