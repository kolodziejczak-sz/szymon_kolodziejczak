import React from 'react';
import PropTypes from 'prop-types';

const Error = ({ text }) => (
  <div className="alert alert-danger" role="alert">
    {text}
  </div>
);

Error.propTypes = {
  text: PropTypes.string
};

export default Error;
