import React from 'react';
import PropTypes from 'prop-types';
import If, { Then, Else } from './If';

export class Column extends React.Component {}

export default class Table extends React.Component {
  getColumns = () => {
    return []
      .concat(this.props.children)
      .filter(c => c && c.type.name === Column.name)
      .map(c => c.props);
  };

  render() {
    const { itemKey, data } = this.props;
    const columns = this.getColumns();

    if (!data || columns.length === 0) return null;

    return (
      <table className="table table-bordered table-hover">
        <thead className="thead-light">
          <tr>
            {columns.map(({ label }, idx) => (
              <th scope="col" key={idx}>
                {label || ''}
              </th>
            ))}
          </tr>
        </thead>
        <tbody>
          <If cond={data.length === 0}>
            <Then>
              <tr>
                <td colSpan={columns.length}>No Results Found</td>
              </tr>
            </Then>
            <Else>
              {data.map(item => (
                <tr key={'row' + item[itemKey]}>
                  {columns.map(({ render, value }, idx) => (
                    <td key={'cell' + idx + item[itemKey]}>
                      {render ? render(item) : item[value] || ''}
                    </td>
                  ))}
                </tr>
              ))}
            </Else>
          </If>
        </tbody>
      </table>
    );
  }
}

Column.propTypes = {
  label: PropTypes.string.isRequired,
  value: PropTypes.any,
  render: PropTypes.func
};

Table.propTypes = {
  children: PropTypes.node
};
