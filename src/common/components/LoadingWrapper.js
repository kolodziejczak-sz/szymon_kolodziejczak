import React from 'react';
import PropTypes from 'prop-types';

const LoadingWrapper = ({ show = false }) => {
  const classNames = [
    'LoadingWrapper',
    'fixed-bottom',
    'py-5',
    'bg-light',
    'text-secondary',
    'fade in',
    show ? 'show' : 'd-none'
  ].join(' ');

  return (
    <div className={classNames}>
      <div className="container">
        <div className="spinner-border" role="status" aria-hidden="true"></div>
        <strong className="ml-3">Loading...</strong>
      </div>
    </div>
  );
};

LoadingWrapper.propTypes = {
  show: PropTypes.bool.isRequired
};

export default LoadingWrapper;
