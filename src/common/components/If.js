import React from 'react';
import PropTypes from 'prop-types';

export class Then extends React.Component {
  render = () => this.props.children;
}

export class Else extends React.Component {
  render = () => this.props.children;
}

export default class If extends React.Component {
  getFirstChildrenComponentByName = name => {
    const component = [].concat(this.props.children).find(c => {
      const componentName = c && c.type.name;
      return componentName === name;
    });
    return component || null;
  };

  renderThenElse = (cond, Then, Else) => (cond ? Then : Else);
  renderChildren = (cond, children) => (cond ? children : null);

  render() {
    const { cond, children } = this.props;
    const ThenComp = this.getFirstChildrenComponentByName(Then.name);
    const ElseComp = this.getFirstChildrenComponentByName(Else.name);
    if (ThenComp) {
      return this.renderThenElse(cond, ThenComp, ElseComp);
    }
    return this.renderChildren(cond, children);
  }
}

Then.propTypes = { children: PropTypes.node.isRequired };

Else.propTypes = { children: PropTypes.node.isRequired };

If.propTypes = {
  cond: PropTypes.bool.isRequired,
  children: PropTypes.oneOfType([
    PropTypes.node.isRequired,
    PropTypes.instanceOf(Then),
    PropTypes.instanceOf(Else)
  ])
};
