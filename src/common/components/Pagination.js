import React from 'react';
import PropTypes from 'prop-types';
import { range, getClassNameWithModifier } from '../utils';
import { If, Button } from '../components';

export default class Pagination extends React.Component {
  render = () => {
    const { pagination, onPageSelect } = this.props;
    const { last, first, current, next, prev } = pagination;
    const delta = 4;
    const pageNumbers = range(Math.max(first, current - delta), Math.min(last, current + delta));
    const isPrevDisabled = !Boolean(prev);
    const isNextDisabled = !Boolean(next);

    return (
      <ul className="pagination justify-content-end">
        <li className={getClassNameWithModifier('page-item', 'disabled')(isPrevDisabled)}>
          <Button
            className="page-link page-prev"
            tabIndex="-1"
            onClick={() => onPageSelect(prev)}
            disabled={isPrevDisabled}
            text="Previous"
          />
        </li>
        {this.renderNumberLinks(pageNumbers, current, onPageSelect)}
        <li className={getClassNameWithModifier('page-item', 'disabled')(isNextDisabled)}>
          <Button
            className="page-link page-next"
            text="Next"
            disabled={isNextDisabled}
            onClick={() => onPageSelect(next)}
          />
        </li>
      </ul>
    );
  };

  renderNumberLinks = (pageNumbers, current, onPageSelect) =>
    pageNumbers.map(link => {
      const isActive = link === current;
      return (
        <li key={link} className={getClassNameWithModifier('page-item', 'active')(isActive)}>
          <Button
            className="page-link page-number"
            text={String(link)}
            disabled={isActive}
            onClick={() => onPageSelect(link)}
          >
            <If cond={isActive}>
              <span className="sr-only">(current)</span>
            </If>
          </Button>
        </li>
      );
    });
}

Pagination.propType = {
  onPageSelect: PropTypes.func.isRequired,
  pagination: PropTypes.shape({
    next: PropTypes.number,
    prev: PropTypes.number,
    current: PropTypes.number.isRequired,
    first: PropTypes.number.isRequired,
    last: PropTypes.number.isRequired
  })
};
