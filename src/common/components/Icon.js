import React from 'react';
import PropTypes from 'prop-types';

const Icon = ({ name, className = '' }) => (
  <i className={`fa ${name} ${className}`} aria-hidden="true" />
);

Icon.propTypes = {
  name: PropTypes.string,
  className: PropTypes.string
};

export default Icon;
