import React from 'react';
import PropTypes from 'prop-types';
import Icon from './Icon';
import If from './If';

const Button = ({ text, icon, children, ...attrs }) => (
  <button type="button" {...attrs}>
    <If cond={!!text}>
      <span>{text}</span>
    </If>
    <If cond={!!icon}>
      <Icon name={icon} />
    </If>
    {children}
  </button>
);

Button.propTypes = {
  text: PropTypes.string,
  icon: PropTypes.string,
  children: PropTypes.node,
  attrs: PropTypes.object
};

export default Button;
