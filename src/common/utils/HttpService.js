import queryString from './queryString';

export const httpMethods = {
  POST: 'POST',
  PUT: 'PUT',
  DELETE: 'DELETE',
  GET: 'GET',
  PATCH: 'PATCH'
};

export default class HttpService {
  static basePath = 'http://localhost:3000/';

  static delete = (...args) => HttpService.makeRequest(httpMethods.DELETE, ...args);
  static get = (...args) => HttpService.makeRequest(httpMethods.GET, ...args);
  static patch = (...args) => HttpService.makeRequest(httpMethods.PATCH, ...args);
  static post = (...args) => HttpService.makeRequest(httpMethods.POST, ...args);
  static put = (...args) => HttpService.makeRequest(httpMethods.PUT, ...args);

  static makeRequest(method = httpMethods.GET, url = '', params) {
    const options = {
      method,
      headers: {
        'Content-Type': 'application/json'
      }
    };

    let resource = HttpService.basePath + url;

    if (params) {
      switch (method) {
        case httpMethods.GET:
        case httpMethods.DELETE:
          const queryParams = queryString.stringify(params);
          if (queryParams) {
            resource += `?${queryParams}`;
          }
          break;
        case httpMethods.PATCH:
        case httpMethods.POST:
        case httpMethods.PUT:
        default:
          options.body = JSON.stringify(params);
          break;
      }
    }
    return window.fetch(resource, options).then(handleFetchResponse);
  }
}

function handleFetchResponse(response) {
  if (!response.ok) {
    throw Error(response.statusText);
  }
  return response;
}
