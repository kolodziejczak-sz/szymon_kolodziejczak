const setLocationQueryParams = params => {
  const location = window.location;
  if (!params) {
    window.history.pushState('', '', location.href.split('?')[0]);
    return;
  }
  const searchString = new URL(location.href).search;
  const searchParams = new URLSearchParams(searchString);
  Object.entries(params).forEach(([k, v]) => {
    if (v === undefined || v === null) {
      searchParams.delete(k);
      return;
    }
    searchParams.set(k, v);
  });
  window.history.pushState('', '', `?${searchParams.toString()}`);
};

const getLocationQueryParams = () => {
  return parse(window.location);
};

const parse = url => {
  if (!url) return new URLSearchParams();
  return new URLSearchParams(new URL(url).search);
};

const stringify = objParam => {
  return Object.entries(objParam)
    .map(([key, value]) => `${key}=${value}`)
    .join('&');
};

const queryString = {
  parse,
  stringify,
  setLocationQueryParams,
  getLocationQueryParams
};

export default queryString;
