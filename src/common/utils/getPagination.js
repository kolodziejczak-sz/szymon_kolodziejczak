import queryString from './queryString';
import parseLinkHeader from './parseLinkHeader';

const getQueryParamFromUrl = (url, param) => {
  const urlSearchParam = queryString.parse(url);
  return urlSearchParam.get(param);
};

export default (res, pageKey) => {
  const linkHeader = res.headers.get('Link');
  if (!linkHeader) return null;

  const paginationInfo = parseLinkHeader(linkHeader);

  const currentPage = Number(getQueryParamFromUrl(res.url, pageKey)) || undefined;
  const pagination = { current: currentPage };

  Object.entries(paginationInfo).forEach(([key, url]) => {
    pagination[key] = Number(getQueryParamFromUrl(url, pageKey));
  });

  if (pagination.first === pagination.last) {
    return null;
  }
  return pagination;
};
