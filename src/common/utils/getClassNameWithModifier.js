const getClassNameWithModifier = (classNameBase, classNameModifier) => modifierCondition => {
  if (modifierCondition) {
    return `${classNameBase} ${classNameModifier}`;
  }
  return classNameBase;
};

export default getClassNameWithModifier;
