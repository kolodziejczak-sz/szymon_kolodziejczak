import queryString from '../queryString';

describe('common/utils:queryString', () => {
  describe('function parse', () => {
    const queryParamKey = 'q';
    const queryParamValue = 'hello';
    const url = `http://website.com?${queryParamKey}=${queryParamValue}`;

    test('changes string to URLSeachParams', () => {
      const res = queryString.parse(url);
      expect(res instanceof URLSearchParams).toBe(true);
    });

    test('return URLSeachParams with params', () => {
      const res = queryString.parse(url);

      expect(res.get(queryParamKey)).toEqual(queryParamValue);
    });
  });

  describe('function stringify', () => {
    test('changes obj to string', () => {
      const res = queryString.stringify({ a: 1 });
      expect(typeof res).toEqual('string');
    });

    test('string has correct keys and values', () => {
      const objParam = {
        a: 1,
        b: 2
      };

      const res = queryString.stringify(objParam);
      const expectedRes = `a=1&b=2`;

      expect(res).toEqual(expectedRes);
    });
  });

  describe('setLocationQueryParams', () => {
    const baseUrl = 'http://www.anypage.com/list';
    const pushState = jest.fn((data, title, url) => {});

    beforeAll(() => {
      global.window = Object.create(window);

      Object.defineProperty(window, 'location', {
        value: { href: baseUrl },
        writable: true
      });

      Object.defineProperty(window, 'history', {
        value: { pushState }
      });
    });

    afterAll(() => {
      global.window.mockClear();
    });

    describe('given null', () => {
      test('calls pushState without query string', () => {
        queryString.setLocationQueryParams(null);

        expect(pushState).toBeCalledWith('', '', baseUrl);
      });
    });

    describe('given object with non-empty values', () => {
      test('calls pushState with queryString', () => {
        queryString.setLocationQueryParams({ q: 1 });

        expect(pushState).toBeCalledWith('', '', '?q=1');
      });
    });

    describe('given object with non-empty values replaces current params', () => {
      test('calls pushState with queryString', () => {
        window.location.href = baseUrl + '?q=2';
        queryString.setLocationQueryParams({ q: 1 });

        expect(pushState).toBeCalledWith('', '', '?q=1');
      });
    });

    describe('given object with empty values', () => {
      test('calls pushState without empty values', () => {
        window.location.href = baseUrl + '?q=2';
        queryString.setLocationQueryParams({ q: undefined });

        expect(pushState).toBeCalledWith('', '', baseUrl);
      });
    });
  });
});
