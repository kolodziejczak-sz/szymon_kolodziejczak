import parseLinkHeader from '../parseLinkHeader';

describe('common/utils:parseLinkHeader', () => {
  test('returns null if header is falsify', () => {
    const link = null;
    expect(parseLinkHeader(link)).toBe(null);
  });

  test('returns object { [rel]: [value] }', () => {
    const val = 'value';
    const rel = 'rel';
    const header = `<${val}>; rel="${rel}"`;
    const expectedObj = { [rel]: val };

    const obj = parseLinkHeader(header);

    expect(obj).toEqual(expectedObj);
  });

  test('parses multiple Link values', () => {
    const header = `<value1>;rel="rel1",<value2>;rel="rel2"`;
    const expectedObj = {
      rel1: 'value1',
      rel2: 'value2'
    };

    const obj = parseLinkHeader(header);

    expect(obj).toEqual(expectedObj);
  });
});
