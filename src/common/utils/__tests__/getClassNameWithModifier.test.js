import getClassNameWithModifier from '../getClassNameWithModifier';

describe('common/utils:getClassNameWithModifier', () => {
  const baseClassName = 'base';
  const modifierClassName = 'modifier';
  const fn = getClassNameWithModifier(baseClassName, modifierClassName);
  let cond;

  test('return base class with modifier when cond is true', () => {
    cond = true;
    expect(fn(cond)).toEqual(`${baseClassName} ${modifierClassName}`);
  });

  test('return base class only when cond is false', () => {
    cond = false;
    expect(fn(cond)).toEqual(`${baseClassName}`);
  });
});
