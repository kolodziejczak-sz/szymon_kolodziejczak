import range from '../range';

describe('common/utils:range', () => {
  test('creates array with valid length', () => {
    const start = 1;
    const stop = 3;

    const arr = range(start, stop);
    const expectedLength = stop - start + 1;

    expect(arr.length).toBe(expectedLength);
  });

  test('creates array with numbers between start and stop', () => {
    const arr = range(1, 3);
    const expectedArray = [1, 2, 3];

    expect(arr).toEqual(expectedArray);
  });
});
