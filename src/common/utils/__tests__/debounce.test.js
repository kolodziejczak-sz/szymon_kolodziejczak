import debounce from '../debounce';

jest.useFakeTimers();

describe('common/utils:debounce', () => {
  test('returns function', () => {
    const noop = () => {};
    const res = debounce(noop, 0);
    expect(typeof res).toBe('function');
  });

  test('execute once', () => {
    const func = jest.fn();
    const debouncedFunc = debounce(func, 1000);

    for (let i = 0; i < 10; i++) {
      debouncedFunc();
    }
    jest.runAllTimers();

    expect(func).toBeCalledTimes(1);
  });
});
