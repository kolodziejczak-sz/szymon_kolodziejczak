import HttpService, { httpMethods } from '../HttpService';

describe('common/utils:HttpService', () => {
  describe('httpMethods', () => {
    let makeRequest;
    let originMakeRequest;

    beforeAll(() => {
      originMakeRequest = HttpService.makeRequest;
      makeRequest = HttpService.makeRequest = jest.fn((method, url, params) => {});
    });

    afterAll(() => {
      HttpService.makeRequest = originMakeRequest;
    });

    test('delete calls makeRequest function with valid method', () => {
      HttpService.delete();
      expect(makeRequest).toHaveBeenCalledWith(httpMethods.DELETE);
    });

    test('get calls makeRequest function with valid method', () => {
      HttpService.get();
      expect(makeRequest).toHaveBeenCalledWith(httpMethods.GET);
    });

    test('post calls makeRequest function with valid method', () => {
      HttpService.post();
      expect(makeRequest).toHaveBeenCalledWith(httpMethods.POST);
    });

    test('put calls makeRequest function with valid method', () => {
      HttpService.put();
      expect(makeRequest).toHaveBeenCalledWith(httpMethods.PUT);
    });

    test('patch calls makeRequest function with valid method', () => {
      HttpService.patch();
      expect(makeRequest).toHaveBeenCalledWith(httpMethods.PATCH);
    });
  });

  describe('makeRequest', () => {
    const basePath = 'base';
    const resourceUrl = 'any';
    let originBasePath;

    beforeAll(() => {
      originBasePath = HttpService.basePath;
      HttpService.basePath = basePath;
      global.fetch = jest.fn().mockImplementation(() => {
        return Promise.resolve({
          ok: true,
          json: () => Promise.resolve()
        });
      });
    });

    afterAll(() => {
      HttpService.basePath = originBasePath;
      global.fetch.mockClear();
    });

    test('isCalling fetch with valid method and url', async () => {
      const method = 'get';
      const options = {
        method,
        headers: {
          'Content-Type': 'application/json'
        }
      };

      HttpService.makeRequest(method, resourceUrl);

      expect(fetch).toBeCalledWith(basePath + resourceUrl, options);
    });

    test('get and delete call assign params to url', async () => {
      const methods = [httpMethods.GET, httpMethods.DELETE];
      const params = { q: 1 };

      for (let method of methods) {
        const expectedOptions = {
          method,
          headers: {
            'Content-Type': 'application/json'
          }
        };

        HttpService.makeRequest(method, resourceUrl, params);

        const expectedUrl = basePath + resourceUrl + '?q=1';
        expect(fetch).toBeCalledWith(expectedUrl, expectedOptions);
      }
    });

    test('post, put, patch calls have params in options.body', async () => {
      const methods = [httpMethods.POST, httpMethods.PUT, httpMethods.PATCH];
      const params = { q: 1 };

      const expectedUrl = basePath + resourceUrl;

      for (let method of methods) {
        const expectedOptions = {
          method,
          headers: {
            'Content-Type': 'application/json'
          },
          body: JSON.stringify(params)
        };

        HttpService.makeRequest(method, resourceUrl, params);

        expect(fetch).toBeCalledWith(expectedUrl, expectedOptions);
      }
    });
  });
});
