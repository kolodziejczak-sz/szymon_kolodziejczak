import getPagination from '../getPagination';

describe('common/utils:getPagination', () => {
  const baseUrl = 'http://www.anypage.com';
  const pageKey = '_page';

  const createLinkHeaderString = (rel, val) => {
    return `<${baseUrl}/list?${pageKey}=${val}>; rel="${rel}"`;
  };

  describe('Link header is not present', () => {
    test('returns null', () => {
      const pageKey = '_page';
      const mockRes = {
        url: baseUrl,
        headers: new Map([])
      };
      const paginationObj = getPagination(mockRes, pageKey);

      expect(paginationObj).toBe(null);
    });
  });

  describe('Link header is present and first and last page are the same', () => {
    test('returns null', () => {
      const mockRes = {
        url: baseUrl,
        headers: new Map([
          [
            'Link',
            `${createLinkHeaderString('first', 1)},
                    ${createLinkHeaderString('last', 1)}`
          ]
        ])
      };
      const paginationObj = getPagination(mockRes, pageKey);

      expect(paginationObj).toBe(null);
    });
  });

  describe('Link header is present and first page is different than last.', () => {
    test('returns pagination object with expected keys and values', () => {
      const keys = ['first', 'prev', 'current', 'next', 'last'];
      const values = [1, 2, 3, 4, 4];
      const mockRes = {
        url: `${baseUrl}/list?${pageKey}=${values[2]}`,
        headers: new Map([
          [
            'Link',
            `${createLinkHeaderString(keys[0], values[0])},
                    ${createLinkHeaderString(keys[1], values[1])},
                    ${createLinkHeaderString(keys[3], values[3])},
                    ${createLinkHeaderString(keys[4], values[4])}`
          ]
        ])
      };

      const paginationObj = getPagination(mockRes, pageKey);
      const paginationObjKeys = Object.keys(paginationObj);
      const paginationObjValues = Object.values(paginationObj);

      expect(paginationObjKeys.sort()).toEqual(keys.sort());
      expect(paginationObjValues.sort()).toEqual(values.sort());
    });
  });

  describe('Current page is present in respond url.', () => {
    test('returns pagination with valid current url', () => {
      const currentPageKey = 'current';
      const currentPage = 2;
      const mockRes = {
        url: `${baseUrl}/list?${pageKey}=${currentPage}`,
        headers: new Map([
          [
            'Link',
            `${createLinkHeaderString('first', 1)},
                    ${createLinkHeaderString('last', 3)}`
          ]
        ])
      };
      const paginationObj = getPagination(mockRes, pageKey);
      expect(paginationObj[currentPageKey]).toEqual(currentPage);
    });
  });

  describe('Current page is not present in respond url.', () => {
    test('returns pagination with current page undefined', () => {
      const currentPageKey = 'current';
      const mockRes = {
        url: baseUrl,
        headers: new Map([
          [
            'Link',
            `${createLinkHeaderString('first', 1)},
                    ${createLinkHeaderString('last', 3)}`
          ]
        ])
      };
      const paginationObj = getPagination(mockRes, pageKey);
      expect(paginationObj[currentPageKey]).toEqual(undefined);
    });
  });
});
