export default linkHeader => {
  if (!linkHeader) return null;

  return linkHeader.split(',').reduce((acc, headerItem) => {
    const parts = headerItem.split(';');
    if (parts.length !== 2) return acc;
    const url = parts[0].replace(/<(.*)>/, '$1').trim();
    const key = parts[1].replace(/rel="(.*)"/, '$1').trim();

    acc[key] = url;
    return acc;
  }, {});
};
