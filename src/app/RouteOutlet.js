import React from 'react';
import PropTypes from 'prop-types';
import { Redirect, Route } from 'react-router-dom';

const RouteOutlet = ({ path, component, redirect }) => {
  if (redirect) {
    return <Redirect from={path} to={redirect} component={component} />;
  }
  return <Route exact key={path} path={path} component={component} />;
};

RouteOutlet.propTypes = {
  path: PropTypes.string.isRequired,
  component: PropTypes.func,
  redirect: PropTypes.string
};

export default RouteOutlet;
