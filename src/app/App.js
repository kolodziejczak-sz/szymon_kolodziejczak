import React, { Component } from 'react';
import Nav from './Nav';
import Routes from './Routes';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Nav />
        <main className="container">
          <Routes />
        </main>
      </div>
    );
  }
}

App.propTypes = {};

export default App;
