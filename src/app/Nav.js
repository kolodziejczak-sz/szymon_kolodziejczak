import React, { Component } from 'react';
import { Link, NavLink } from 'react-router-dom';
import { Button } from '../common/components';
import { getClassNameWithModifier } from '../common/utils';

class Nav extends Component {
  state = {
    isMenuCollapsed: false
  };

  toggleMenu = () => {
    this.setState({
      isMenuCollapsed: !this.state.isMenuCollapsed
    });
  };

  render = () => {
    const { isMenuCollapsed } = this.state;
    return (
      <nav className="navbar navbar-expand-sm navbar-dark bg-primary mb-3">
        <Link to="/" className="navbar-brand">
          Sonalake Task
        </Link>
        <Button
          onClick={this.toggleMenu}
          className={getClassNameWithModifier('navbar-toggler', 'collapsed')(isMenuCollapsed)}
          aria-expanded={isMenuCollapsed}
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon" />
        </Button>

        <div
          className={getClassNameWithModifier('collapse navbar-collapse', 'show')(isMenuCollapsed)}
        >
          <ul className="navbar-nav">
            {this.renderNavItem('/characters', 'CharacterList')}
            {this.renderNavItem('/characters/add', 'Add character')}
          </ul>
        </div>
      </nav>
    );
  };

  renderNavItem = (to, label) => (
    <li className="nav-item">
      <NavLink exact to={to} className="nav-link">
        {label}
      </NavLink>
    </li>
  );
}

Nav.propTypes = {};

export default Nav;
