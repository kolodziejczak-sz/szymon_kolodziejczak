import React from 'react';
import { Switch } from 'react-router-dom';
import RouteOutlet from './RouteOutlet';
import { CharacterList, CharacterForm } from '../characters';

export const routes = [
  { name: 'CharacterForm', path: '/characters/add', component: CharacterForm },
  { name: 'CharacterList', path: '/characters', component: CharacterList },
  { path: '/', redirect: '/characters' }
];

const Routes = () => (
  <Switch>
    {routes.map(route => (
      <RouteOutlet exact key={route.path} {...route} />
    ))}
  </Switch>
);

export default Routes;
