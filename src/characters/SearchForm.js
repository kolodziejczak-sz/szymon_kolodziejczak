import React from 'react';
import PropTypes from 'prop-types';
import { debounce } from '../common/utils';
import { TextInput } from '../common/form';

class SearchForm extends React.Component {
  constructor(props) {
    super(props);
    this.onSearch = debounce(this.onSearch, 200);
  }

  onSearch = this.props.onSearch;

  render() {
    return (
      <div className="SearchForm">
        <TextInput
          id="searchInput"
          placeholder="Search..."
          defaultValue={this.props.value}
          onKeyUp={e => this.onSearch(e.currentTarget.value)}
        ></TextInput>
        <label htmlFor="searchInput" className="sr-only">
          Search
        </label>
      </div>
    );
  }
}

SearchForm.propTypes = {
  defaultValue: PropTypes.string,
  onSearch: PropTypes.func
};

export default SearchForm;
