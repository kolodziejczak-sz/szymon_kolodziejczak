import {
  fetchCharacters,
  addCharacter,
  fetchSpecies,
  fetchFormData,
  pageKey,
  searchTextKey,
  defaultPage,
  defaultQuery
} from '../characters.api';
import HttpService from '../../common/utils/HttpService';

jest.mock('../../common/utils/HttpService');

const mockResponseAsOk = (method, data) => {
  HttpService[method].mockReturnValue(
    Promise.resolve({
      ok: true,
      headers: new Map([]),
      json: () => Promise.resolve(data)
    })
  );
};

const mockResponseAsError = method => {
  HttpService[method].mockReturnValue(
    Promise.reject({
      ok: false,
      headers: new Map([]),
      json: () => Promise.resolve()
    })
  );
};

describe('characters API', () => {
  describe('fetchCharacters', () => {
    const charactersUrl = 'characters';

    describe('with ok respond from Api', () => {
      describe('given no params', () => {
        test('calls HttpService.get with default page', () => {
          const defaultParams = { [pageKey]: defaultPage };
          mockResponseAsOk('get');
          fetchCharacters();

          expect(HttpService.get).toBeCalledWith(charactersUrl, defaultParams);
        });
      });

      describe('given params', () => {
        test('calls HttpService.get with valid params', () => {
          const params = {
            [searchTextKey]: 'aaa',
            [pageKey]: 2
          };
          mockResponseAsOk('get');
          fetchCharacters(params[pageKey], params[searchTextKey]);

          expect(HttpService.get).toBeCalledWith(charactersUrl, params);
        });
      });

      describe('arguments context', () => {
        test('if SearchText is provided returns fields associated with search', async () => {
          mockResponseAsOk('get');
          const res = await fetchCharacters(1, 'aaa');
          const resKeys = Object.keys(res);
          const expectedKeys = ['searchText', 'searchList', 'searchPagination'];

          expect(resKeys.sort()).toEqual(expectedKeys.sort());
        });

        test('if SearchText is empty providing fields associated with no search', async () => {
          mockResponseAsOk('get');
          const res = await fetchCharacters(1);
          const resKeys = Object.keys(res);
          const expectedKeys = ['list', 'pagination'];

          expect(resKeys.sort()).toEqual(expectedKeys.sort());
        });
      });
    });

    describe('with error respond from Api', () => {
      test('returns object with error', async () => {
        mockResponseAsError('get');

        const res = await fetchCharacters();

        expect(res.error).toEqual(expect.anything());
      });
    });
  });

  describe('fetchSpecies', () => {
    describe('with ok respond from Api', () => {
      test('calls HttpService.get with valid arguments', () => {
        const speciesUrl = 'species';
        mockResponseAsOk('get');

        fetchSpecies();

        expect(HttpService.get).toBeCalledWith(speciesUrl);
      });

      test('returns mapped list to { label, value } entity', async () => {
        const values = ['a', 'b', 'c'];
        mockResponseAsOk('get', values);

        const respond = await fetchSpecies();

        expect(respond.map(i => i.value)).toEqual(values);
        expect(respond.map(i => i.label)).toEqual(values);
      });
    });

    describe('with error respond from Api', () => {
      test('returns object with error', async () => {
        mockResponseAsError('get');

        const res = await fetchSpecies();

        expect(res.error).toEqual(expect.anything());
      });
    });
  });

  describe('fetchFormData', () => {
    describe('with ok respond from Api', () => {
      test('returns formData object', async () => {
        mockResponseAsOk('get', { species: [] });
        const expectedKeys = ['formData'];

        const res = await fetchFormData();
        const resKeys = Object.keys(res);

        expect(resKeys.sort()).toEqual(expectedKeys.sort());
      });
    });

    describe('with error respond from Api', () => {
      test('returns object with error', async () => {
        mockResponseAsError('get');

        const res = await fetchFormData();

        expect(res.error).toEqual(expect.anything());
      });
    });
  });

  describe('addCharacter', () => {
    describe('with ok respond from Api', () => {
      test('returns new character', async () => {
        const body = { name: 'aaa', species: 'human', gender: 'male' };
        const bodyWithId = { ...body, id: 1 };
        mockResponseAsOk('post', bodyWithId);

        const res = await addCharacter(body);

        expect(res).toEqual(bodyWithId);
      });
    });

    describe('with error respond from Api', () => {
      test('returns object with error', async () => {
        mockResponseAsError('post');
        const body = {};
        const res = await addCharacter(body);

        expect(res.error).toEqual(expect.anything());
      });
    });
  });
});
