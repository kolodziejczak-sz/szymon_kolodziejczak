import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import { addCharacter, fetchFormData } from './characters.api';
import { Button, Error, If, LoadingWrapper, Title } from '../common/components';
import {
  Form,
  FormControl,
  SelectInput,
  TextInput,
  RadioGroupInput,
  validators
} from '../common/form';

class CharacterForm extends Component {
  state = {
    redirectToList: false,
    isPending: false,
    isSaving: false,
    error: null,
    formData: null
  };

  componentDidMount = () => {
    this.getFormData();
  };

  getFormData = () => {
    this.setState({
      ...this.state,
      isPending: true,
      error: null
    });
    fetchFormData().then(this.handleFormDataResponse);
  };

  handleFormDataResponse = response => {
    this.setState({
      ...this.state,
      error: null,
      ...response,
      isPending: false
    });
  };

  redirectToList = () => {
    this.setState({
      ...this.state,
      redirectToList: true
    });
  };

  onSubmit = ({ isValid, value }) => {
    if (!isValid) return;
    this.setState({
      ...this.state,
      isSaving: true
    });
    addCharacter(value).then(res => {
      this.setState({
        ...this.state,
        isSaving: false,
        ...(res.error ? res : { redirectToList: true })
      });
    });
  };

  render = () => {
    if (this.state.redirectToList) return <Redirect to="/characters" />;

    const { formData, isPending, isSaving, error } = this.state;
    const { character = {}, species = [], genders = [] } = formData || {};

    return (
      <div className="CharacterForm">
        <Title text="Character Form" />
        <LoadingWrapper show={isPending} />
        <If cond={!!error}>
          <Error text={error} />
        </If>
        <Form onSubmit={this.onSubmit}>
          <FormControl
            id="charName"
            name="name"
            label="Name"
            validator={validators.required}
            defaultValue={character.name}
          >
            <TextInput placeholder="Enter name" />
          </FormControl>
          <FormControl
            name="species"
            id="charSpecies"
            label="Species"
            validator={validators.required}
            defaultValue={character.species}
          >
            <SelectInput options={species} />
          </FormControl>
          <FormControl
            name="homeworld"
            id="charHomeworld"
            label="Homeworld"
            defaultValue={character.homeworld}
          >
            <TextInput placeholder="Enter homeworld" />
          </FormControl>
          <FormControl
            name="gender"
            label="Gender"
            validator={validators.required}
            defaultValue={character.gender}
          >
            <RadioGroupInput list={genders} />
          </FormControl>
          {this.renderButtons(this.redirectToList, this.onSubmit, isSaving)}
        </Form>
      </div>
    );
  };

  renderButtons = (onCancel, onSubmit, loading) => (
    <div className="btn-group float-right" role="group" aria-label="Form buttons">
      <Button className="btn btn-light" onClick={onCancel} text="See list" />
      <Button
        type="submit"
        className="btn btn-primary"
        text="Submit"
        onClick={onSubmit}
        disabled={loading}
      >
        <If cond={loading}>
          <div className="spinner-border spinner-border-sm ml-1" role="status">
            <span className="sr-only">Loading...</span>
          </div>
        </If>
      </Button>
    </div>
  );
}

CharacterForm.propTypes = {};

export default CharacterForm;
