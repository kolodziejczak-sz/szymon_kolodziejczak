import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { queryString } from '../common/utils';
import {
  fetchCharacters,
  pageKey,
  searchTextKey,
  defaultPage,
  defaultQuery
} from './characters.api';
import SearchForm from './SearchForm';
import {
  Button,
  Column,
  Error,
  If,
  LoadingWrapper,
  Pagination,
  Table,
  Title
} from '../common/components';

const { setLocationQueryParams, getLocationQueryParams } = queryString;

class CharacterList extends Component {
  state = {
    list: null,
    searchText: '',
    searchList: null,
    searchPagination: null,
    pagination: null,
    isPending: false,
    error: null
  };

  componentDidMount = () => {
    const queryParams = getLocationQueryParams();
    const page = queryParams.get(pageKey) || defaultPage;
    const searchText = queryParams.get(searchTextKey) || defaultQuery;
    this.getCharacters(page, searchText);
  };

  onPageSelected = page => {
    setLocationQueryParams({
      [pageKey]: page
    });
    this.getCharacters(page, this.state.searchText);
  };

  onSearchText = text => {
    if (text) {
      setLocationQueryParams({
        [searchTextKey]: text,
        [pageKey]: undefined
      });
      this.getCharacters(1, text);
      return;
    }
    setLocationQueryParams(null);
    this.setState({
      ...this.state,
      searchText: '',
      searchPagination: null,
      searchList: null
    });
    const isInitialListFetched = this.state.list !== null;
    if (!isInitialListFetched) {
      this.getCharacters();
    }
  };

  getCharacters(page = 1, searchText = '') {
    this.setState({
      ...this.state,
      searchText,
      error: null,
      isPending: true
    });
    fetchCharacters(page, searchText).then(this.handleCharactersResponse);
  }

  handleCharactersResponse = response => {
    this.setState({
      ...this.state,
      error: null,
      ...response,
      isPending: false
    });
  };

  render() {
    const {
      pagination,
      list,
      searchList,
      searchText,
      searchPagination,
      error,
      isPending
    } = this.state;
    const isSearchMode = searchText.length > 0;
    const displayList = isSearchMode ? searchList : list;
    const displayPagination = isSearchMode ? searchPagination : pagination;

    return (
      <div className="CharacterList">
        <LoadingWrapper show={isPending} />
        <Title text="Character list" />
        <If cond={!!error}>
          <Error text={error} />
        </If>
        {this.renderControls()}
        <Table data={displayList} itemKey="id">
          <Column label="Id" value="id" />
          <Column label="Name" value="name" />
          <Column label="Species" value="species" />
          <Column label="Gender" value="gender" />
          <Column label="Homeworld" value="homeworld" />
          <Column
            label="Actions"
            render={_ => (
              <div className="btn-group btn-group-sm" role="group" aria-label="Actions">
                <Button className="btn btn-secondary" icon="fa-pencil" text="edit" />
                <Button className="btn btn-danger" icon="fa-trash-o" text="remove" />
              </div>
            )}
          />
        </Table>
        <If cond={!!displayPagination}>
          <nav aria-label="Data grid navigation">
            <Pagination pagination={displayPagination} onPageSelect={this.onPageSelected} />
          </nav>
        </If>
      </div>
    );
  }

  renderControls = () => (
    <div className="row">
      <div className="col-sm-6">
        <SearchForm value={this.state.searchText} onSearch={this.onSearchText} />
      </div>

      <div className="col-sm-6 text-sm-right">
        <Link to="/characters/add" className="btn btn-primary mb-3">
          Add New
        </Link>
      </div>
    </div>
  );
}

CharacterList.propTypes = {};

export default CharacterList;
