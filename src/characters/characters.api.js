import { HttpService, getPagination } from '../common/utils/';

export const pageKey = '_page';
export const searchTextKey = 'q';
export const defaultPage = 1;
export const defaultQuery = '';

export const fetchFormData = async () => {
  const species = await fetchSpecies();
  const character = {};
  const genders = [
    { label: 'Male', value: 'male' },
    { label: 'Female', value: 'female' },
    { label: 'n/a', value: 'n/a' }
  ];

  if (species.error) {
    return { error: species.error };
  }

  const formData = { character, species, genders };
  return { formData };
};

export const fetchSpecies = async () => {
  try {
    const speciesRes = await HttpService.get('species');
    const speciesList = await speciesRes.json();
    if (speciesList && speciesList.length > 0) {
      return speciesList.map(o => ({ label: o, value: o.toLowerCase() }));
    }
    return speciesList;
  } catch (_) {
    return { error: 'Characters could not be retrieved. Try again later.' };
  }
};

export const addCharacter = async character => {
  try {
    const res = await HttpService.post(`characters`, character);
    const resData = await res.json();
    return resData;
  } catch (_) {
    return { error: 'New hero creation failed. Please try again later.' };
  }
};

export const fetchCharacters = async (page = defaultPage, searchText = defaultQuery) => {
  try {
    const res = await HttpService.get('characters', {
      [pageKey]: page,
      ...(searchText && { [searchTextKey]: searchText })
    });
    const list = await res.json();
    const pagination = getPagination(res, pageKey);

    if (searchText) {
      return {
        searchText,
        searchList: list,
        searchPagination: pagination
      };
    }
    return {
      list,
      pagination
    };
  } catch (_) {
    return { error: 'Characters could not be retrieved. Try again later.' };
  }
};
