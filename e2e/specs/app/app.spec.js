import puppeteer from 'puppeteer';
import { routes } from '../../../src/app/Routes';

const createUrl = (route) => {
  return (process.env.TEST_HOST).trim() + route.path.slice(1)
}

describe('app', () => {

  describe('go to homepage', async () => {

    let browser, page;

    beforeAll(async () => {
      browser = await puppeteer.launch({
        headless: false
      });
      
      page = await browser.newPage();
      page.emulate({
        viewport: {
          width: 500,
          height: 2400
        },
        userAgent: ''
      });
      const homepageUrl = createUrl({ path: '/' });
      await page.goto(homepageUrl);
    })

    afterAll(async () => {
      browser.close();
    })

    test('renders content correctly', async () => {
      await page.waitForSelector('.App');
  
      const html = await page.$eval('.navbar-brand', e => e.innerHTML);
      expect(html).toBe('Sonalake Task');
    });

    test('redirects to list', async () => {
      const listRoute = routes.find(r => r.name === 'CharacterList');
      const expectedUrl = createUrl(listRoute);

      expect(page.url()).toEqual(expectedUrl);
    });
  })
});
