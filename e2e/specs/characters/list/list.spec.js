import puppeteer from 'puppeteer';
import getData from '../../../../config/jsonServerTest';
import { routes } from '../../../../src/app/Routes';
import { pageKey } from '../../../../src/characters/characters.api';

const waitForLoading = async (page) => {
  await page.waitForSelector('.LoadingWrapper:not(.d-none)');
  await page.waitForSelector('.LoadingWrapper.d-none');
}

const createUrl = (route) => {
  return (process.env.TEST_HOST).trim() + route.path.slice(1)
}

describe('CharacterList', () => {

  let browser, page;
  const listRoute = routes.find(r => r.name === 'CharacterList');
  const formRoute = routes.find(r => r.name === 'CharacterForm');

  beforeAll(async () => {
    browser = await puppeteer.launch({
      headless: false
    });
    
    page = await browser.newPage();
    page.emulate({
      viewport: {
        width: 500,
        height: 2400
      },
      userAgent: ''
    });
    const url = createUrl(listRoute);
    await page.goto(url);
    await waitForLoading(page);
  })

  afterAll(async () => {
    browser.close();
  })

  test('renders pagination if data.length > 10', async () => {
    const dataLength = getData().characters.length;
    const shouldRenderPagination = (dataLength > 10)

    const pagination = await page.$('.pagination');

    expect((shouldRenderPagination && Boolean(pagination))).toBe(true);
  });

  test('pagination changes url', async () => {
    const nextBtn = await page.$('.pagination .page-next');

    await nextBtn.click();
    await waitForLoading(page);    

    const isPageKeyInUrl = (page.url().indexOf(pageKey) !== -1);
    expect(isPageKeyInUrl).toBe(true);
  });

  test('clicking add button should redirect to form', async () => {
    const addBtn = await page.$('.btn.btn-primary.mb-3');
    
    addBtn.click();
    await waitForLoading(page);
    
    const expectedUrl = createUrl(formRoute);
    expect(page.url()).toBe(expectedUrl);
  });
});
