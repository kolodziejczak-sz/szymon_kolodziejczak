const data = {
  species: [
    "Aleena",
    "Besalisk",
    "Cerean",
  ],
  characters: [
    {
      "id": 1,
      "name": "Luke Skywalker",
      "species": "Human",
      "gender": "male",
      "homeworld": "Tatooine"
    },
    {
      "id": 2,
      "name": "C-3PO",
      "species": "Droid",
      "gender": "n/a",
      "homeworld": "Tatooine"
    },
    {
      "id": 3,
      "name": "R2-D2",
      "species": "Droid",
      "gender": "n/a",
      "homeworld": "Naboo"
    },
    {
      "id": 4,
      "name": "Darth Vader",
      "species": "Human",
      "gender": "male",
      "homeworld": "Tatooine"
    },
    {
      "id": 5,
      "name": "Leia Organa",
      "species": "Human",
      "gender": "female",
      "homeworld": "Alderaan"
    },
    {
      "id": 6,
      "name": "Obi-Wan Kenobi",
      "species": "Human",
      "gender": "male",
      "homeworld": "Stewjon"
    },
    {
      "id": 7,
      "name": "Anakin Skywalker",
      "species": "Human",
      "gender": "male",
      "homeworld": "Tatooine"
    },
    {
      "id": 8,
      "name": "Chewbacca",
      "species": "Wookiee",
      "gender": "male",
      "homeworld": "Kashyyyk"
    },
    {
      "id": 9,
      "name": "Han Solo",
      "species": "Human",
      "gender": "male",
      "homeworld": "Corellia"
    },
    {
      "id": 10,
      "name": "Yoda",
      "species": "Yoda's species",
      "gender": "male",
      "homeworld": ""
    },
    {
      "id": 11,
      "name": "Sheev Palpatine",
      "species": "Human",
      "gender": "male",
      "homeworld": "Naboo"
    },
  ]
} 

module.exports = () => data;